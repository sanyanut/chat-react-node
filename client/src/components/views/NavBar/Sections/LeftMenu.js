import React from "react";
import { Menu } from "antd";

function LeftMenu(props) {
  return (
    <Menu mode={props.mode}>
      <Menu.Item key="mail" className="ant-menu-item">
        <a href="/">Home</a>
      </Menu.Item>
      <Menu.Item key="chat" className="ant-menu-item">
        <a href="/chat">Chat</a>
      </Menu.Item>
    </Menu>
  );
}

export default LeftMenu;
