import React, { Component } from "react";
import { Form, Icon, Input, Button, Row, Col } from "antd";
import io from "socket.io-client";
import { connect } from "react-redux";
import moment from "moment";
import { getChats, afterPostMessage } from "../../../_actions/chat_actions";
import ChatCard from "./Sections/ChatCard";
export class ChatPage extends Component {
  state = {
    chatMessage: "",
  };

  componentDidMount() {
    let server = "http://localhost:5000";

    this.props.dispatch(getChats());

    this.socket = io(server);

    this.socket.on("Output Chat Message", (messageFromBackEnd) => {
      console.log(messageFromBackEnd);
      this.props.dispatch(afterPostMessage(messageFromBackEnd));
    });
  }

  hanleSearchChange = (e) => {
    this.setState({
      chatMessage: e.target.value,
    });
  };

  renderCards = () =>
    this.props.chats.chats &&
    this.props.chats.chats.map((chat) => <ChatCard key={chat._id} {...chat} />);

  submitChatMessage = (e) => {
    e.preventDefault();

    let chatMessage = this.state.chatMessage,
      userId = this.props.user.userData._id,
      userName = this.props.user.userData.name,
      userImage = this.props.user.userData.image,
      nowTime = moment(),
      type = "Text";

    this.socket.emit("Input Chat Message", {
      chatMessage,
      userId,
      userName,
      userImage,
      nowTime,
      type,
    });
    this.setState({ chatMessage: "" });
  };

  render() {
    return (
      <>
        <div style={{ maxWidth: "1000px", margin: "0 auto" }}>
          <div className="infinite-container" style={{ height: "500px" }}>
            {this.props.chats && this.renderCards()}
            <div
              ref={(node) => {
                this.messagesEnd = node;
              }}
              style={{ float: "left", clear: "both" }}
            />
          </div>

          <Row>
            <Form layout="inline" onSubmit={this.submitChatMessage}>
              <Col span={18}>
                <Input
                  id="message"
                  placeholder="Message"
                  type="text"
                  value={this.state.chatMessage}
                  onChange={this.hanleSearchChange}
                />
              </Col>

              <Col span={4}>
                <Button
                  type="primary"
                  style={{ width: "100%" }}
                  onClick={this.submitChatMessage}
                  htmlType="submit"
                >
                  Send
                </Button>
              </Col>
            </Form>
          </Row>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    chats: state.chat,
  };
};

export default connect(mapStateToProps)(ChatPage);
