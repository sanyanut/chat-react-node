import React from "react";

function LandingPage() {
  return (
    <>
      <div className="app">
        <span style={{ fontSize: "4rem" }}>Home</span>
      </div>
    </>
  );
}

export default LandingPage;
